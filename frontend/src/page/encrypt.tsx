import {
  Button,
  createMuiTheme,
  Grid,
  TextField,
  ThemeProvider,
  Typography,
} from "@material-ui/core";
import { lightBlue } from "@material-ui/core/colors";
import axios from "axios";
import React, { useState } from "react";
import { Url, Header } from "../api/config";

const spacer = (num: number) => {
  return <div style={{ marginTop: String(num) + "rem" }}></div>;
};

const theme = createMuiTheme({
  palette: {
    primary: lightBlue,
  },
});

export function Encrypt() {
  const [kirim, setkirim] = useState({
    data: [0],
    pk: {
      e: 0,
      n: 0,
    },
  });

  const [input, setinput] = useState("");
  const [filein, setfilein] = useState({
    name: "",
    type: "",
  });
  const [hasil, sethasil] = useState({
    cipher: [0],
    time: "",
  });
  const [tampilkan, settampilkan] = useState(undefined);

  function getFileFromInput(file: File): Promise<any> {
    return new Promise(function (resolve, reject) {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = function () {
        resolve(reader.result);
        if (reader.result !== null) {
          let persiapan: string = String(reader.result);
          let val = JSON.parse(persiapan);
          setkirim({
            pk: {
              e: val.e,
              n: val.n,
            },
            data: kirim.data,
          });
        }
      };
      reader.readAsBinaryString(file);
    });
  }

  function handleFileChange(event: any) {
    event.persist();
    Array.from(event.target.files).forEach((file: any) => {
      getFileFromInput(file)
        .then((binary) => {})
        .catch(function (reason) {
          event.target.value = "";
        });
    });
  }

  function getFileFromInput2(file: File): Promise<any> {
    return new Promise(function (resolve, reject) {
      const reader = new FileReader();
      reader.onerror = reject;
      reader.onload = function () {
        resolve(reader.result);
        if (reader.result !== null) {
          let persiapan: string = String(reader.result);
          let a = [...persiapan].map((s, idx) => s.charCodeAt(0));
          setkirim({
            data: a,
            pk: kirim.pk,
          });
        }
      };
      reader.readAsBinaryString(file);
    });
  }

  function manageUploadedFile(binary: String, file: File) {
    setfilein({
      name: "enc_" + file.name,
      type: file.type,
    });
  }

  function handleFileChange2(event: any) {
    event.persist();
    Array.from(event.target.files).forEach((file: any) => {
      getFileFromInput2(file)
        .then((binary) => {
          manageUploadedFile(binary, file);
        })
        .catch(function (reason) {
          event.target.value = "";
        });
    });
  }

  function submit() {
    if (filein.name === "") {
      setfilein({
        name: "enc_default.txt",
        type: "text",
      });
      let array_hex_integer = input.split(",").map(function (item) {
        return parseInt(item, 10);
      });
      axios
        .post(
          Url + "/encrypt",
          { ...kirim, data: array_hex_integer },
          {
            headers: Header,
          }
        )
        .then((response: any) => {
          sethasil({
            cipher: response.data.cipher,
            time: response.data.time,
          });
          var b = response.data.cipher
            .map(function (x: any) {
              return x.toString(16);
            })
            .toString();
          settampilkan(b);
        })
        .catch((error) => {
          alert(error.message);
        });
    } else
      axios
        .post(
          Url + "/encrypt",
          { ...kirim },
          {
            headers: Header,
          }
        )
        .then((response: any) => {
          sethasil({
            cipher: response.data.cipher,
            time: response.data.time,
          });
          var b = response.data.cipher
            .map(function (x: any) {
              return x.toString(16);
            })
            .toString();
          settampilkan(b);
        })
        .catch((error) => {
          alert(error.message);
        });
  }

  const downloadchip = () => {
    const blob = new Blob([JSON.stringify({ data: hasil.cipher }, null, 2)], {
      type: filein.type,
    });
    saveAs(blob, filein.name);
  };

  return (
    <>
      <Grid container justify="space-between">
        <Grid item style={{ margin: "auto 0" }}>
          <Typography variant="h5" component="p">
            Configuration
          </Typography>
          {spacer(0.6)}
          <Typography variant="h6" component="p">
            Enter Public Key
          </Typography>
          {spacer(0.6)}
          <input
            accept="*"
            style={{ color: "#C9CADA", textTransform: "none" }}
            id="file"
            multiple={true}
            type="file"
            onChange={(e: any) => handleFileChange(e)}
          />
          {spacer(0.6)}
          <Typography variant="h6" component="p">
            Enter Message
          </Typography>
          {spacer(0.6)}
          <Grid container justify="space-between">
            <Grid item style={{ margin: "auto 0" }}>
              <input
                accept="*"
                style={{ color: "#C9CADA", textTransform: "none" }}
                id="file"
                multiple={true}
                type="file"
                onChange={(e: any) => handleFileChange2(e)}
              />
            </Grid>
            <Grid item style={{ margin: "auto 0", paddingLeft: "3rem" }}>
              <Typography variant="body1" component="p">
                or
              </Typography>
            </Grid>
            <Grid item style={{ margin: "auto 0", paddingLeft: "3rem" }}>
              <ThemeProvider theme={theme}>
                <TextField
                  id="outlined-basic"
                  multiline
                  label="Message"
                  variant="outlined"
                  InputLabelProps={{
                    style: { color: "#C9CADA" },
                  }}
                  value={input}
                  onChange={(e: any) => {
                    setinput(e.target.value);
                  }}
                />
              </ThemeProvider>
            </Grid>
          </Grid>
          {spacer(0.6)}
          <Button
            variant="outlined"
            style={{
              color: "#C9CADA",
              textTransform: "none",
              marginBottom: "1rem",
            }}
            onClick={submit}
          >
            Submit
          </Button>
        </Grid>
        <Grid item style={{ margin: "auto 0" }}>
          <Typography variant="h5" component="p">
            Preview
          </Typography>
          {spacer(1)}
          {hasil.cipher[0] !== 0 ? (
            <>
              <Typography variant="h6" component="p">
                Ciphertext
              </Typography>
              <Typography
                variant="body1"
                component="p"
                style={{
                  whiteSpace: "nowrap",
                  textOverflow: "ellipsis",
                }}
              >
                {tampilkan}
              </Typography>
              {spacer(1)}
              <Typography variant="body2" component="p">
                {"time execution = " + hasil.time}
              </Typography>
              {spacer(0.6)}
              <Button
                variant="outlined"
                style={{
                  color: "#C9CADA",
                  textTransform: "none",
                  marginBottom: "1rem",
                }}
                onClick={downloadchip}
              >
                Download Ciphertext
              </Button>
            </>
          ) : null}
        </Grid>
      </Grid>
    </>
  );
}
