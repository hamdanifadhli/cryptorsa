import {
  Button,
  createMuiTheme,
  Grid,
  TextField,
  ThemeProvider,
  Typography,
} from "@material-ui/core";
import { lightBlue } from "@material-ui/core/colors";
import axios from "axios";
import React, { useState } from "react";
import { Url, Header } from "../api/config";
import { saveAs } from "file-saver";

const spacer = (num: number) => {
  return <div style={{ marginTop: String(num) + "rem" }}></div>;
};

const theme = createMuiTheme({
  palette: {
    primary: lightBlue,
  },
});

export function Keygen() {
  const [n, setn] = useState(undefined);
  const [resp, setresp] = useState({
    show: false,
    time: "",
    pk: {
      e: 0,
      n: 0,
    },
    sk: {
      d: 0,
      n: 0,
    },
  });

  const submit = () => {
    let url = Url + "/keygen";
    if (n !== undefined && n !== "") {
      url = url + "/" + n;
    } else {
      url = Url + "/keygen";
    }
    axios
      .post(
        url,
        {},
        {
          headers: Header,
        }
      )
      .then((response: any) => {
        setresp({ ...response.data, show: true });
      })
      .catch((error) => {
        alert(error.message);
      });
  };

  const downloadPub = async () => {
    const blob = new Blob([JSON.stringify(resp.pk, null, 2)], {
      type: "application/json",
    });
    saveAs(blob, "key.pub");
  };

  const downloadPriv = async () => {
    const blob = new Blob([JSON.stringify(resp.sk, null, 2)], {
      type: "application/json",
    });
    saveAs(blob, "ppk.pri");
  };

  return (
    <>
      <Grid container justify="space-between">
        <Grid item style={{ margin: "auto 0" }}>
          <Typography variant="h5" component="p">
            Configuration
          </Typography>
          {spacer(1)}
          <ThemeProvider theme={theme}>
            <TextField
              id="outlined-basic"
              label="N bits (default 7) min 4"
              variant="outlined"
              InputLabelProps={{
                style: { color: "#C9CADA" },
              }}
              value={n}
              onChange={(e: any) => {
                if (!e.target.value.match(/[^$,.\d]/)) {
                  setn(e.target.value);
                }
              }}
            />
            <br />
            {spacer(1)}
            <Button
              variant="outlined"
              style={{ color: "#C9CADA", textTransform: "none" }}
              onClick={submit}
            >
              Submit
            </Button>
          </ThemeProvider>
        </Grid>
        <Grid item style={{ margin: "auto 0", textAlign: "right" }}>
          <Typography variant="h5" component="p">
            Preview
          </Typography>
          {spacer(1)}
          {resp.show ? (
            <>
              <Typography variant="h6" component="p">
                Public Key
              </Typography>
              <Typography variant="body1" component="p">
                {"< e= " + resp.pk.e + ", n= " + resp.pk.n + " >"}
              </Typography>
              {spacer(0.6)}
              <Typography variant="h6" component="p">
                Secret Key
              </Typography>
              <Typography variant="body1" component="p">
                {"< e= " + resp.sk.d + ", n= " + resp.sk.n + " >"}
              </Typography>
              {spacer(0.6)}
              <Typography variant="body2" component="p">
                {"time execution = " + resp.time}
              </Typography>
              {spacer(1)}
              <div style={{ flexGrow: 1 }}>
                <Button
                  variant="outlined"
                  style={{
                    color: "#C9CADA",
                    textTransform: "none",
                    marginRight: "1rem",
                  }}
                  onClick={downloadPub}
                >
                  Download Public Key
                </Button>
                <Button
                  variant="outlined"
                  style={{ color: "#C9CADA", textTransform: "none" }}
                  onClick={downloadPriv}
                >
                  Download Private Key
                </Button>
              </div>
            </>
          ) : null}
        </Grid>
      </Grid>
    </>
  );
}
