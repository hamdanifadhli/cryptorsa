import { Hidden } from "@material-ui/core";
import React from "react";

export function Home() {
  return (
    <>
      <div style={{ margin: "auto", textAlign: "center" }}>
        <Hidden smDown>
          <img src="./img/welcome/hero.png" alt="" style={{ height: "24vw" }} />
        </Hidden>
        <Hidden mdUp>
          <img src="./img/welcome/hero.png" alt="" style={{ height: "35vw" }} />
        </Hidden>
      </div>
    </>
  );
}
