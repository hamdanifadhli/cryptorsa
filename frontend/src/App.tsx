import React, { useState } from "react";
import { Appibar } from "./components/appbar";
import { Content } from "./components/content";

function App() {
  const [page, setpage] = useState(0);
  return (
    <div style={{ backgroundColor: "#232649" }}>
      <Appibar page={page} setpage={setpage} />
      <section
        style={{
          backgroundColor: "#232649",
          height: "83.5vh",
          color: "#A9ABB8",
          padding: "3rem",
        }}
      >
        <Content page={page} setpage={setpage} />
      </section>
    </div>
  );
}

export default App;
