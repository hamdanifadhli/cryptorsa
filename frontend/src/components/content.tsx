import {
  Button,
  ButtonGroup,
  Card,
  CardContent,
  Container,
  makeStyles,
  Typography,
} from "@material-ui/core";
import React from "react";
import { Title } from "../constant/mainc";
import { Decrypt } from "../page/decrypt";
import { Encrypt } from "../page/encrypt";
import { Home } from "../page/home";
import { Keygen } from "../page/keygen";

const useStyles = makeStyles({
  root: {
    maxWidth: 1260,
    margin: "auto",
    backgroundColor: "#29658A",
    color: "white",
  },
});

export function Content(props: any) {
  const classes = useStyles();
  const { page, setpage } = props;
  return (
    <>
      <Card className={classes.root}>
        <CardContent>
          {page !== 4 ? (
            <ButtonGroup
              variant="text"
              color="primary"
              style={{
                marginBottom: "1rem",
              }}
            >
              <Button
                style={{
                  color: "white",
                  outlineColor: "white",
                  textTransform: "none",
                  backgroundColor: "#24264C",
                }}
                onClick={() => {
                  setpage(1);
                }}
              >
                Key Generation
              </Button>
              <Button
                style={{
                  color: "white",
                  outlineColor: "white",
                  textTransform: "none",
                  backgroundColor: "#24264C",
                }}
                onClick={() => {
                  setpage(2);
                }}
              >
                Encryption
              </Button>
              <Button
                style={{
                  color: "white",
                  outlineColor: "white",
                  textTransform: "none",
                  backgroundColor: "#24264C",
                }}
                onClick={() => {
                  setpage(3);
                }}
              >
                Decryption
              </Button>
            </ButtonGroup>
          ) : (
            <Button
              style={{
                color: "white",
                outlineColor: "white",
                textTransform: "none",
                backgroundColor: "#24264C",
                marginBottom: "1rem",
              }}
              onClick={() => {
                setpage(0);
              }}
            >
              Back to Home Screen
            </Button>
          )}

          <Container>
            <Typography
              variant="h4"
              component="p"
              style={{ textAlign: "center" }}
            >
              {Title[page]}
            </Typography>
            {page === 0 ? (
              <Home />
            ) : page === 1 ? (
              <Keygen />
            ) : page === 2 ? (
              <Encrypt />
            ) : page === 3 ? (
              <Decrypt />
            ) : null}
          </Container>
        </CardContent>
      </Card>
    </>
  );
}
