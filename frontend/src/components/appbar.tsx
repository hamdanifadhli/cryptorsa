import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  createStyles,
  makeStyles,
  Theme,
} from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      backgroundColor: "#232649",
      color: "white",
    },
    title: {
      flexGrow: 1,
    },
  })
);

export function Appibar(props: any) {
  const classes = useStyles();
  const { page, setpage } = props;
  return (
    <>
      <AppBar position="static" className={classes.root} elevation={0}>
        <Toolbar>
          <Typography
            variant="h5"
            className={classes.title}
            onClick={() => {
              setpage(0);
            }}
          >
            Cryptoboy
          </Typography>

          {/* <Button
            style={{ color: "#A9ABB8", fontWeight: "bold" }}
            onClick={() => {
              setpage(4);
            }}
          >
            About Us
          </Button> */}
        </Toolbar>
      </AppBar>
    </>
  );
}
