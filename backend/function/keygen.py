import random
from function.helper import findInverse,isPrime

def keygen(mxb):
    maxbit = mxb
    maxval = int(2**maxbit)

    p,q = 0,0

    count = 0

    rando = int(random.randint(200,700)/100)
    while count < rando :
        if isPrime(maxval) :
            if count == 0 :
                p = maxval
            elif count == rando-1 :
                q = maxval
            count = count + 1
        maxval = maxval - 1

    phi = (p-1)*(q-1)
    n = p*q
    e = (n//q)*int(random.randint(500,1000)/100)

    while not isPrime(e) :
        e = e + 1

    d = findInverse(e,phi)

    return ((e,n),(d,n))

