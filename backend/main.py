from function.keygen import keygen
from function.decrypt import decrypt
from function.encrypt import encrypt
from flask import Flask,request
from flask_cors import CORS, cross_origin
import time

_start_time = time.time_ns()
def tic():
    global _start_time
    _start_time = time.time_ns()


def tac():
    return (time.time_ns() - _start_time)/1000000

app = Flask(__name__)
CORS(app)

@app.route('/keygen', methods=['POST'])
@app.route('/keygen/<val>', methods=['POST'])
@cross_origin() 
def keygeneration(val = 7):
    if request.method == 'POST':
        tic()
        pk,sk = keygen(int(val))
        time = tac()
        return {
            "pk": {
                "e": hex(pk[0]),
                "n": hex(pk[1]),
            },
            "sk": {
                "d": hex(sk[0]),
                "n": hex(sk[1]),
            },
            "time": str(time) + " ms",
        }

@app.route("/encrypt", methods=["POST"])
@cross_origin() 
def encryption():
    if request.method == 'POST':
        request_data = request.get_json()
        ret = []
        tic()
        for data in request_data['data']:
            ret.append(encrypt(data,(int(request_data['pk']['e'],0),int(request_data['pk']['n'],0))))
        time = tac()
        return {
            "cipher": ret,
            "time": str(time) + " ms",
        }

@app.route("/decrypt", methods=["POST"])
@cross_origin() 
def decryption():
    if request.method == 'POST':
        request_data = request.get_json()
        ret = []
        tic()
        for data in request_data['data']:
            ret.append(decrypt(data,(int(request_data['sk']['d'],0),int(request_data['sk']['n'],0))))
        time = tac()
        return {
            "data": ret,
            "time": str(time) + " ms",
        }   

if __name__ == "__main__" :
    app.run(debug=False)
    


# pk,sk = keygen(7)
# print(pk, sk)
# chip = encrypt(100,pk)
# print(chip)
# dec = decrypt(chip,sk)
# print(dec)